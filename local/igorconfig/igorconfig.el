(provide 'igorconfig)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Abbrevs

(add-hook 'text-mode-hook
          '(lambda ()
             (abbrev-mode)
             (define-abbrev text-mode-abbrev-table "i" "I")
             (define-abbrev text-mode-abbrev-table "i'm" "I'm")
             (define-abbrev text-mode-abbrev-table "i've" "I've")
             (define-abbrev text-mode-abbrev-table "i'd" "I'd")
             (define-abbrev text-mode-abbrev-table "i'll" "I'll")))

(add-hook 'org-mode-hook
          '(lambda ()
             (abbrev-mode)
             (define-abbrev org-mode-abbrev-table "i" "I")
             (define-abbrev org-mode-abbrev-table "i'm" "I'm")
             (define-abbrev org-mode-abbrev-table "i've" "I've")
             (define-abbrev org-mode-abbrev-table "i'd" "I'd")
             (define-abbrev org-mode-abbrev-table "i'll" "I'll")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Miscellaneous

(defun dfc ()
  "Changes default-fill-column to 64 or 78."
  (interactive)
  (setq default-fill-column (if (= default-fill-column 64) 78 64))
  (message "default-fill-column is now %d" default-fill-column))

;; TODO: send pull request to Spacemacs
;; http://camdez.com/blog/2013/11/14/emacs-show-buffer-file-name/
(defun spacemacs/show-and-copy-buffer-filename ()
  "Show the full path to the current file in the minibuffer."
  (interactive)
  (let ((file-name (if (eq major-mode 'eshell-mode) (eshell/pwd) (buffer-file-name))))
    (if file-name
        (progn
          (message file-name)
          (kill-new file-name))
      (error "Buffer not visiting a file"))))

(defun igorconfig-custom-diff-colors ()
  "update the colors for diff faces"
  (set-face-attribute
   'diff-added nil :foreground "#9FF09F" :background nil)
  (set-face-attribute
   'diff-removed nil :foreground "#F08F8F" :background nil))

(defun igorconfig-custom-repl-colors ()
  "update the colors for diff faces"
  (set-face-attribute
   'cider-repl-stdout-face nil :foreground "#BBBBBB" :background nil))

(defun save-eval-and-switch-to-repl ()
  (interactive)
  (if (buffer-modified-p)
      (progn
        (save-buffer)
        (cider-interactive-eval "(println \"\n################################################################################\n\")") 
        (cider-eval-buffer)
        (run-with-timer 0.05 nil 'cider-switch-to-repl-buffer))
    (cider-switch-to-repl-buffer)))

(defun g (dir)
  (insert (format "pushd %s" (cadr (member dir g-directories))))
  (eshell-send-input))

(defun gg (dir)
  (kill-new (cadr (member dir g-directories))))

;; Полнейший изврат, в следующий раз попробуй вот что: https://github.com/rejeep/f.el
(defun ga ()
  (switch-to-buffer nil)
  (let ((dir buffer-file-name))
    (eshell)
    (insert "pushd " dir)
    (search-backward "/")
    (kill-line)
    (eshell-send-input)))

(defun mknow (&optional prefix)
  (interactive)
  (insert "mkdir " (if prefix (concat prefix "-") "") (current-date))
  (eshell-send-input))

(defun now()
  (interactive)
  (insert (current-date)))

(defun mv_ (file)
  (interactive)
  (let ((newfile (if (= (aref file 0) ?_) (substring file 1) (concat (string ?_) file))))
    (insert "mv " file " " newfile)
    (eshell-send-input)))

(defun cdddr (lst)
  (cdr (cdr (cdr lst))))

(defun caddr (lst)
  (car (cdr (cdr lst))))

(defun date->string (date)
  (let ((d (cdddr date)))
    (format "%d-%02d-%02d" (caddr d) (cadr d) (car d))))

(defun time->string (date)
  (format "%02d:%02d:%02d" (caddr date) (cadr date) (car date)))

(defun current-date ()
  (date->string (decode-time)))

(defun current-date-and-time ()
  (format
   "%s %s"
   (date->string (decode-time))
   (time->string (decode-time))))

(defun spit (filename str)
  (switch-to-buffer "*the-new-buffer*")
  (insert str)
  (write-file filename)
  (kill-buffer (current-buffer)))

;; Has a nasty side effect of killing the buffer even if it was
;; opened before the call to slurp. todo?
(defun slurp (filename)
  (save-excursion
    (find-file filename)
    (let ((result (substring-no-properties (thing-at-point 'buffer))))
      (kill-buffer)
      result)))

(defun line-at-point ()
  (save-excursion
    (beginning-of-line)
    (let ((pnt (point)))
      (end-of-line)
      (buffer-substring-no-properties pnt (point)))))

(defun string-at-point (&optional n)
  (if n
      (buffer-substring-no-properties (point) (+ (point) n))
    (save-excursion
      (let ((pnt (point)))
        (end-of-line)
        (buffer-substring-no-properties pnt (point))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Fringe

(defun tt-toggle-fringe ()
  (interactive)
  (if (eq fringe-mode 320)
      (fringe-mode '(4 . 4))
    (fringe-mode 320)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Books

(defun =mark ()
  (interactive)
  (beginning-of-line)
  (let ((beg (point)))
    (end-of-line)
    (while (< (- (point) beg) 71)
      (insert " "))
    (insert " == ")
    (evil-append -1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; zsh

(defun tt-shell ()
  (interactive)
  (if (eq system-type 'gnu/linux)
      (if (get-buffer "*terminal<1>*")
          (switch-to-buffer "*terminal<1>*")
        (multi-term))
      (let ((explicit-shell-file-name "d:/cygwin/bin/zsh"))
        (call-interactively 'shell)))
  (delete-other-windows))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Workspaces

(defun tt-buffer-shell? ()
  (member major-mode '(shell-mode term-mode)))

(defun tt-buffer-normal? ()
  (not (or (tt-buffer-shell?)
           (member major-mode '(help-mode compilation-mode)))))

(defun tt-buffer-goto-normal-buffer ()
  (let ((started-with (buffer-name)))
    (other-window 1)
    (while (and (not (tt-buffer-normal?))
                (not (eq started-with (buffer-name))))
      (other-window 1))
    (when (not (tt-buffer-normal?))
      (spacemacs/alternate-buffer))))

;; normal buffer and nothing more
(defun ws-0 ()
  (interactive)
  (tt-buffer-goto-normal-buffer)
  (delete-other-windows))

(defun tt-path= (a b)
  (if (eq system-type 'gnu/linux)
      (string= a b)
    (eq t (compare-strings a nil nil b nil nil 't))))

;; shell
(defun ws-shell ()
  (interactive)
  (let ((buff (current-buffer))
        (curr-dir default-directory))
    (tt-shell)
    (split-window-vertically)
    (other-window 1)
    (switch-to-buffer buff)
    (other-window 1)
    (end-of-buffer)
    (evil-insert 0)))

(defun tt-toggle-shell ()
  (interactive)
  (if (not (tt-buffer-shell?))
      (ws-shell)
    (ws-0)))

(defun tt-shell-send-input ()
  (if (eq major-mode 'shell-mode)
      (comint-send-input)
    (term-send-input)))

(defun tt-popd ()
  (interactive)
  (end-of-buffer)
  (insert "popd")
  (tt-shell-send-input))

(defun tt-pushd ()
  (interactive)
  (other-window 1)
  (let ((dir default-directory))
    (other-window -1)
    (end-of-buffer)
    (insert (concat "pushd " dir))
    (tt-shell-send-input)))
