(setq igorconfig-packages
      '((igorconfig :location local)))

(defun dirtrack-mode-for-win32 ()
  (when (not (eq system-type 'gnu/linux))
    (shell-dirtrack-mode 0)
    (set-variable 'dirtrack-list '("~~~> \\(.*\\)> *" 1 nil))
    (dirtrack-mode 1)))

(defun igorconfig/init-igorconfig ()
  (use-package igorconfig
    :defer 0
    :config
    (progn
      (set-default 'fill-column 64)
      (set-default 'truncate-lines t)
      (setq-default evil-escape-key-sequence ",.")
      ;; Org-mode
      (eval-after-load 'org
        '(progn
           (add-to-list 'org-link-frame-setup '(file . find-file))
           (setq org-cycle-emulate-tab nil)))
      (eval-after-load 'org-pomodoro
        '(progn
           (set-face-foreground 'org-pomodoro-mode-line "#303030")
           (setq org-pomodoro-play-sounds nil)
           (setq org-pomodoro-format "%s")
           (setq org-pomodoro-short-break-format "Break %s")))
      (eval-after-load 'diff-mode '(igorconfig-custom-diff-colors))
      ;; Ya snippet!
      (yas-global-mode)
      (add-to-list 'yas-snippet-dirs "/home/igor/.emacs.d/private/snippets")
      (yas-reload-all)
      ;; w3m
      (evil-leader/set-key "o+" 'browse-zhome)
      (eval-after-load 'w3m
        '(progn
           (define-key w3m-mode-map (kbd "+") 'zz-goto-source)))
      ;; Clojure
      (eval-after-load 'clojure-mode
        '(progn
           (evil-leader/set-key-for-mode 'clojure-mode
             "mfd" 'cider-format-defun)
           (define-key clojure-mode-map (kbd "M-n") 'save-eval-and-switch-to-repl)))
      (eval-after-load 'cider-repl
        '(progn
           (setq cider-repl-display-in-current-window 't)
           (define-key cider-repl-mode-map (kbd "M-n") 'cider-switch-to-last-clojure-buffer)
           (igorconfig-custom-repl-colors)))
      ;; Shell
      (setq term-buffer-maximum-size 65536)
      (add-hook 'shell-mode-hook
                '(lambda ()
                   (dirtrack-mode-for-win32)
                   (define-key shell-mode-map (kbd "M-h") 'tt-pushd)))
      (add-hook 'term-mode-hook
                '(lambda ()
                   (define-key term-raw-map (kbd "C-l") nil)
                   (define-key term-raw-map (kbd "M-f") nil)
                   (define-key term-raw-map (kbd "M-b") nil)
                   (add-to-list 'term-bind-key-alist '("M-h" . tt-pushd))))
      ;; Ivy
      (setq ivy-virtual-abbreviate 'full)
      (add-to-list 'ivy-initial-inputs-alist '(counsel-M-x . ""))
      ;; Keybindings
      (evil-leader/set-key "o=" '=mark)
      (evil-leader/set-key "oh" '=ws-0)
      (global-set-key (kbd "M-f") 'counsel-find-file)
      (global-set-key (kbd "M-b") 'ivy-switch-buffer)
      (global-set-key (kbd "M-s") 'tt-toggle-shell)
      ;; Miscellaneous
      (when (eq system-type 'gnu/linux)
        (setenv "PAGER" "/bin/cat"))
      (setq avy-keys '(?h ?t ?n ?s ?q ?j ?k ?x ?b ?m ?w ?v ?z ?p ?y ?f ?g ?c ?r ?l ?a ?o ?e ?u))
      (setq dired-listing-switches "-alh")
      (setq ls-lisp-format-time-list '("%Y-%m-%d %H:%M"))
      (setq dotspacemacs-large-file-size 8)
      ;; TODO: do something with mwheel scrolling
      ;; Say gl hf
      (message "gl hf"))))
